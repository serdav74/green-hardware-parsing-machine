from pony.orm import db_session, flush, commit, ObjectNotFound
from selenium.common.exceptions import WebDriverException

from db import Shop, ShopItem
from update_manager import UpdateType, Update
from decimal import Decimal
from datetime import datetime


class JobFailed(Exception):
    pass


class ShopParser:
    def __init__(self):
        self.startup_time = None
        self.success = None
        self.elapsed_time = None

    @classmethod
    def get_shop_name(cls):
        """Shop name (used in logging and database)"""
        raise NotImplementedError

    shop_id = None

    request_headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0',
    }

    @property
    def logger(self):
        """A logging.Logger object"""
        raise NotImplementedError

    @classmethod
    def register_shop(cls):
        """A method to add or update shop entity in database"""
        raise NotImplementedError

    def parse_list(self):
        self.startup_time = datetime.now()
        try:
            items = self._parse_list()
            self.success = True
            self.elapsed_time = datetime.now() - self.startup_time
            return items
        except (OSError, WebDriverException, JobFailed):
            self.success = False
            raise

    def _parse_list(self):
        """A function to parse item list"""
        raise NotImplementedError

    @db_session
    def save_items(self, item_list, price_change_threshold=None):
        if price_change_threshold is None:
            price_change_threshold = Decimal(0.1)
        else:
            price_change_threshold = Decimal(price_change_threshold)

        self.logger.info("Saving items to database...")
        updates = []

        shop = Shop[self.__class__.shop_id]

        # remove deleted items from database
        existing_ids = set([data[0] for data in item_list])
        for item in ShopItem.select(lambda s: s.shop == shop):
            if item.id not in existing_ids:
                updates.append(Update(
                    type=UpdateType.ITEM_REMOVED,
                    item_id=None,
                    old_name=item.name,
                    old_price=item.price
                ))
                item.delete()
        commit()

        for data in item_list:
            try:
                item = ShopItem[data[0]]
                if abs(item.price - data[3]) >= price_change_threshold:
                    old_price = item.price
                    item.price = data[3]
                    updates.append(Update(type=UpdateType.PRICE_CHANGED, item_id=item.id, old_price=old_price))
            except ObjectNotFound:
                item = ShopItem(id=data[0], name=data[1], link=data[2], price=data[3], shop=shop)
                updates.append(Update(type=UpdateType.NEW_ITEM, item_id=item.id))

        self.logger.info("Updates found: {}".format(len(updates)))
        return updates

    @classmethod
    @db_session
    def register_shop(cls):
        shop = Shop.get(name=cls.get_shop_name())
        if not shop:
            shop = Shop(name=cls.get_shop_name())
        flush()
        cls.shop_id = shop.id
