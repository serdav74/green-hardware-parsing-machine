import json
import logging
import sys


def setup_logging(level):
    logger = logging.getLogger('app')
    handler = logging.StreamHandler(stream=sys.stdout)
    formatter = logging.Formatter(fmt="{asctime:>16} | {name:24} | {levelname:8} | {message}", style='{')

    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    logger.setLevel(level)
    return logger


class JsonConfig:
    def __init__(self, filename="config.json", template=None):
        """
        Creates a JsonConfig object from a file.
        :param filename: Filename to load from and save to.
        :param template: Default config which will be used if given file does not exist.
        """
        self.filename = filename
        self.storage = dict()
        self.reload(template)
        self.logger = logging.getLogger('app.config')

    def reload(self, template=None):
        try:
            with open(self.filename, 'rt') as f:
                self.storage = dict(json.load(f))
        except FileNotFoundError:
            if template:
                self.storage = template
                # we do this so that template object changes would not affect our config storage
                # and we also need to save it right away
                self.save()
                self.reload()
            else:
                raise
        except (OSError, json.JSONDecodeError):
            self.logger.error("Failed to read config.", exc_info=1)
            raise

    def save(self):
        with open(self.filename, 'wt') as f:
            json.dump(self.storage, f)

    def __contains__(self, item):
        return item in self.storage.keys()

    def __getitem__(self, item):
        return self.storage[item]

    def __setitem__(self, key, value):
        self.storage[key] = value
