from enum import Enum
from itertools import groupby
import logging

from pony.orm import db_session, ObjectNotFound

from db import ShopItem


class UpdateType(Enum):
    NEW_ITEM = 1
    PRICE_CHANGED = 2
    ITEM_REMOVED = 3
    ITEM_BLACKLISTED = 101
    ITEM_UNBLACKLISTED = 102
    BLACKLISTING_FAILED = 103


class Update:
    def __init__(self, type, item_id=None, **kwargs):
        self.type = type
        self.ignored = False
        self.info = kwargs
        self.item_id = item_id
        if self.item_id:
            self.item = self.fetch_item(item_id)
        else:
            self.item = None
            if type in [UpdateType.NEW_ITEM, UpdateType.PRICE_CHANGED]:
                raise ValueError("item_id is required.")

    @staticmethod
    @db_session
    def fetch_item(id):
        item = ShopItem[id]
        item.load()
        return item

    def __str__(self):
        return self.format(type_emoji=False, name_width=40, link=False)

    emoji_map = {
        UpdateType.NEW_ITEM: "➕",
        UpdateType.PRICE_CHANGED: "🏷️",
        UpdateType.ITEM_REMOVED: "➖",
        UpdateType.ITEM_BLACKLISTED: "⛔",
        UpdateType.ITEM_UNBLACKLISTED: "🔔",
        UpdateType.BLACKLISTING_FAILED: "🐞"
    }

    type_strings_map = {
        UpdateType.NEW_ITEM: "New item",
        UpdateType.PRICE_CHANGED: "Price change",
        UpdateType.ITEM_REMOVED: "Item removed",
        UpdateType.ITEM_BLACKLISTED: "Item blacklisted:",
        UpdateType.ITEM_UNBLACKLISTED: "Item unblacklisted:",
        UpdateType.BLACKLISTING_FAILED: "Blacklisting failed:"
    }

    def format(self, type_text=True, type_emoji=True, link=True, name_width=100):
        s = ("{emoji}{type}{name:" + str(name_width) + "}").format(
            emoji=(self.emoji_map[self.type] + " " if type_emoji else ""),
            type=(self.type_strings_map[self.type] + ": " if type_text else ""),
            name=self.item.name[:name_width] if self.item else self.info['old_name']
        )

        if link and self.item:
            if self.item:
                link = '\n' + self.item.link
            else:
                link = '\n' + "(link unavailable)"
        else:
            link = ""

        if self.type is UpdateType.NEW_ITEM:
            s += " [{price}]{link}".format(
                price=self.item.price,
                link=link
            )
        elif self.type is UpdateType.PRICE_CHANGED:
            s += " [{new_price} ({price_change:+})]{link}".format(
                # old_price=self.info['old_price'],
                new_price=self.item.price,
                price_change=(self.item.price - self.info['old_price']),
                link=link
            )
        elif self.type is UpdateType.ITEM_REMOVED:
            s += " [{old_price}]".format(old_price=self.info['old_price'])
        elif self.type in UpdateType:
            s += ""
        else:
            s += " (unknown update type: {})".format(self.type.name)

        return s


class UpdateFilter:
    def __init__(self, type_whitelist=None, id_blacklist=None, name_blacklist=None):
        self.logger = logging.getLogger('app.filter')

        if not type_whitelist:
            self.logger.warning('False-y whitelist! You will probably get no updates.')

        self.type_whitelist = type_whitelist
        self.id_blacklist = id_blacklist
        self.name_blacklist = name_blacklist
        self._reset()

    def _reset(self):
        self.updates_passed = 0
        self.updates_blacklisted = 0  # updates that do not fit blacklists
        self.updates_ignored = 0  # updates that do not fit whitelists

    def _filter(self, updates):
        self._reset()
        for update in updates:
            if update.type not in self.type_whitelist:
                self.updates_ignored += 1
                continue
            if update.item_id in self.id_blacklist:
                self.updates_blacklisted += 1
                continue
            # todo: implement name blacklisting
            self.updates_passed += 1
            yield update

    def format(self, updates):
        strings = []
        emoji = Update.emoji_map
        types = Update.type_strings_map

        filtered_updates = list(self._filter(updates))
        for type, values in groupby(sorted(filtered_updates, key=lambda u: u.type.value), lambda u: u.type):
            if type not in self.type_whitelist:
                amount = sum(1 for update in values if not update.ignored)
                strings.append("{} {}: {} items".format(
                    emoji[type],
                    types[type],
                    amount
                ))
            else:
                strings.append("{} {}:".format(emoji[type], types[type]))
                strings.extend(update.format(type_text=False, type_emoji=False) for update in values)

        strings.append("{} updates blacklisted.".format(self.updates_blacklisted))
        return "\n\n".join(strings)
