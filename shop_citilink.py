import json
import logging
from decimal import Decimal

import bs4
import requests

from base_classes import ShopParser


class CitilinkShop(ShopParser):
    def __init__(self, **kwargs):
        super().__init__()
        self._logger = logging.getLogger('app.parser.citilink')
        self._logger.info("Parser initialized")

    shop_id = None

    @classmethod
    def get_shop_name(cls):
        return "citilink.ru"

    @property
    def logger(self):
        return self._logger

    def _parse_list(self):
        self.logger.info("Preparing to parse \"{}\".".format(self.get_shop_name()))

        url = r"https://www.citilink.ru/discount/computers_and_notebooks/parts/"

        r = requests.get(url)
        soup = bs4.BeautifulSoup(r.text, 'html5lib')

        # get current city
        city_div = soup.find('span', 'city-popup_toggle-button__js')
        city = city_div.text.strip()
        self.logger.info("Current city: {}".format(city))

        # parse items
        content = soup.find('div', class_='main_content_inner')
        item_divs = content.find_all('div', class_='product-card')
        item_data = []
        for item_div in item_divs:
            try:
                item_data.append(self._extract_item_data(item_div))
            except json.JSONDecodeError:
                self.logger.warning("Could not decode json data: \'{}\'".format(item_div['data-params']))

        self.logger.info("Parsed {} items.".format(len(item_data)))
        return item_data

    @staticmethod
    def _extract_item_data(div):
        data = json.loads(div['data-params'])

        id = data['id']
        name = data['shortName']
        link = div.find('div', class_='product-card__name').find('a')['href']
        price = Decimal(data['price'])

        return id, name, link, price
