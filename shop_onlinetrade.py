import logging
import re
from decimal import Decimal

import bs4

from base_classes import ShopParser


class OnlineTradeShop(ShopParser):
    def __init__(self, web_driver, **kwargs):
        super().__init__()
        self._logger = logging.getLogger('app.parser.onlinetrade')
        self._logger.info("Parser initialized")
        self.driver = web_driver

    shop_id = None

    @classmethod
    def get_shop_name(cls):
        return "onlinetrade.ru"

    @property
    def logger(self):
        return self._logger

    def _parse_list(self):
        self.driver.delete_all_cookies()

        self.logger.info("Preparing to parse \"{}\".".format(self.get_shop_name()))
        self.logger.debug("Fetching page 0...")
        url = r"https://www.onlinetrade.ru/discount/komplektuyushchie_dlya_pk-c390/?c=42"
        self.driver.get(url)

        self.driver.implicitly_wait(10)  # seconds

        # get current city
        el = self.driver.find_element_by_css_selector("span.header__citySelectLink")
        city = el.text.strip()
        self.logger.info("Current city: {}".format(city))

        goods_data = []

        loop = True

        while loop:
            # extract data
            goods_box = self.driver.find_element_by_css_selector("div.goods__items")
            more_goods = self._parse_list_html(goods_box.get_attribute('innerHTML'))
            goods_data.extend(more_goods)
            self.logger.debug("Found {} items".format(len(more_goods)))

            self.logger.debug("Seeking for next page...")
            # seek to new page
            paginator = self.driver.find_element_by_css_selector("div.paginator__links")
            last_button = paginator.find_elements_by_css_selector("a")[-1]
            if last_button.get_attribute('innerHTML') == '→':
                last_button.click()
            else:
                self.logger.debug("Looks like that was the last page. Breaking loop.")
                loop = False

        self.logger.info("Parsed {} items.".format(len(goods_data)))

        return goods_data

    def _parse_list_html(self, inner_html):
        soup = bs4.BeautifulSoup(inner_html, 'html.parser')

        data = []
        goods = soup.findAll(class_='indexGoods__item')
        for item in goods:
            try:
                item_data = self._extract_item_data(item)
                data.append(item_data)
            except NotImplementedError:
                self.logger.warning("Could not parse item: {}".format(str(item)))
        return data

    @staticmethod
    def _extract_item_data(div):
        base_url = "https://www.onlinetrade.ru"
        link = base_url + div.find('a')['href']
        name = div.find('a', class_='indexGoods__item__name').text
        price_text = div.find('div', class_='indexGoods__item__price').find('span', class_='regular').text[:-1].strip()

        id_match = re.search(r'(\d+-\d+)\.html$', link)
        id = id_match.group(1)
        price = Decimal(price_text.replace(' ', ''))
        return id, name, link, price
