import datetime
import re

from vk_api.exceptions import ApiError

from base_classes import JobFailed
from selenium_tools import construct_driver
from shop_citilink import CitilinkShop
from shop_dns import DNSShop
from shop_onlinetrade import OnlineTradeShop
from update_manager import Update, UpdateType, UpdateFilter, ObjectNotFound
from utils import JsonConfig, setup_logging
from vk_bot import Bot


class App:
    def __init__(self, config, parser_classes):
        self.config = config
        self.available_parsers = parser_classes
        self.logger = setup_logging(config['log_level'])
        self.startup = datetime.datetime.now()
        self.time_elapsed = None
        self.parsers = list()
        self.updates = list()

        self.logger.info("Starting the app...")

        self.whitelist = [type_ for type_ in UpdateType if type_.name in config['parsers']['update_types']]
        self.blacklist = JsonConfig("blacklist.json", {'id': [], 'name': []})

        self.vk_bot = Bot(access_token=config['vk']['token'], group=config['vk']['group_id'])

    def run(self):
        self.logger.info('Constructing default Selenium driver...')
        driver = construct_driver()
        self.logger.info('Done!')

        try:
            # register shops to database
            for cls in self.available_parsers:
                cls.register_shop()

            self._create_parsers(web_driver=driver)
            self._get_updates()
            self._generate_reports()
        finally:
            driver.quit()

    def _create_parsers(self, **kwargs):
        self.parsers = [
            cls(**kwargs)
            for cls in self.available_parsers
            if cls.get_shop_name() not in self.config['parsers']['disabled']
        ]

    def _get_updates(self):
        for parser in self.parsers:
            try:
                items = parser.parse_list()
                self.updates.extend(
                    parser.save_items(
                        items,
                        price_change_threshold=self.config['parsers']['min_price_change']
                    )
                )
            except JobFailed:
                self.logger.error("Failed to parse {}!".format(parser.get_shop_name()), exc_info=1)
                continue

    @property
    def failed_jobs(self):
        return sum(1 for _ in self.parsers if not _.success)

    def _generate_reports(self):
        self.time_elapsed = datetime.datetime.now() - self.startup

        update_filter = UpdateFilter(
            type_whitelist=self.whitelist,
            id_blacklist=self.blacklist['id'],
            name_blacklist=self.blacklist['name']
        )

        updates_message = update_filter.format(self.updates)

        message = "Jobs done in {elapsed}{failed}, updates: {passed}/{total}.\n\n".format(
            elapsed=self.time_elapsed,
            passed=update_filter.updates_passed,
            total=len(self.updates),
            failed="; {} failed".format(self.failed_jobs) if self.failed_jobs > 0 else ""
        )

        self.logger.info(message.strip('\n'))

        if update_filter.updates_passed > 0:
            message += updates_message
            users = self.config['vk']['whitelisted_users']
            try:
                self.vk_bot.send_long_messages(users, message, split_by='\n\n')
            except ApiError:
                pass

        try:
            self.logger.info("Sending a comment...")
            message = "Yes ({:%d %b, %H:%M}, {}u, {}/{} jobs).".format(
                self.startup,
                len(self.updates),
                len(self.parsers) - self.failed_jobs,
                len(self.parsers)
            )
            self.vk_bot.post_wall_comment(message, post_id=self.config['vk']['report_post_id'])
        except ApiError:
            pass

    def _parse_inbox_messages(self):
        messages = self.vk_bot.get_last_messages(self.config['vk']['whitelisted_users'])
        log_pattern = re.compile(r'/log (.*)')
        blacklist_add_pattern = re.compile(r'/b\+ (id|name) ([\S]+)')

        for message in messages:
            text = message['text']

            match = log_pattern.match(text)
            if match:
                self.logger.debug(match.group(1))

            match = blacklist_add_pattern.match(text)
            if match:
                entry_type = match.group(1)
                entry_info = match.group(2)

                self.logger.info(f"Control message: b+ type={entry_type} id={repr(entry_info)}")

                if entry_type == 'id':
                    try:
                        item = Update.fetch_item(entry_info)
                    except ObjectNotFound:
                        self.updates.append(
                            Update(type=UpdateType.BLACKLISTING_FAILED, item_id=entry_info, old_name=entry_info)
                        )
                        continue
                else:
                    raise NotImplementedError()

                self.blacklist[entry_type].append(entry_info)
                self.updates.append(Update(type=UpdateType.ITEM_BLACKLISTED, item_id=entry_info))





def main():
    available_parsers = [CitilinkShop, DNSShop, OnlineTradeShop]
    config = JsonConfig("config.json")

    app = App(config, available_parsers)
    app.run()


if __name__ == "__main__":
    main()
