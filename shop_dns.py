import logging
import re
from decimal import Decimal
from time import sleep

import bs4

from base_classes import ShopParser


class DNSShop(ShopParser):
    def __init__(self, web_driver, **kwargs):
        super().__init__()
        self._logger = logging.getLogger('app.parser.dns')
        self._logger.info("Parser initialized")
        self.driver = web_driver

    shop_id = None

    @classmethod
    def get_shop_name(cls):
        return "dns-shop.ru"

    @property
    def logger(self):
        return self._logger

    def _parse_list(self):
        self.driver.delete_all_cookies()

        self.logger.info("Preparing to parse \"{}\".".format(self.get_shop_name()))
        url = r"https://www.dns-shop.ru/catalog/markdown/?category=17a89aab16404e77-17a8914916404e77-17a89a0416404e77-17a899cd16404e77"
        self.driver.get(url)

        self.driver.add_cookie({'name': 'city-path', 'value': 'chelyabinsk'})
        self.driver.implicitly_wait(10)  # seconds

        # scroll the page down

        last_height = self.driver.execute_script("return document.body.scrollHeight")
        while last_height < 200000:
            self.logger.debug("Scrolling down to {} px...".format(last_height))
            # scroll to bottom
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            # calculate new page height and compare with last scroll height
            new_height = self.driver.execute_script("return document.body.scrollHeight")
            if new_height == last_height:
                # try to find a button
                button_present = self.driver.execute_script('return $(\'a[data-role=\"next-load-block-button\"]:visible > span\').length')
                if button_present:
                    # perform a click
                    self.driver.execute_script('$(\'a[data-role=\"next-load-block-button\"]:visible > span\').trigger("click")')
                    self.logger.debug("Button is visible; clicking and waiting for data...")
                    sleep(5)
                else:
                    self.logger.debug("Button is not visible; that's the end.")
                    break
            last_height = new_height

        # get current city
        el = self.driver.find_element_by_css_selector('div.city-select')
        html = el.get_attribute('innerHTML')
        soup = bs4.BeautifulSoup(html, 'html5lib')
        city = soup.text.strip()
        self.logger.info("Current city: {}".format(city))

        # get the item container
        el = self.driver.find_element_by_css_selector('div.catalog-category')
        html = el.get_attribute('innerHTML')
        soup = bs4.BeautifulSoup(html, 'html5lib')
        products = soup.find_all('div', class_='product')
        items_data = []
        for product in products:
            items_data.append(self._extract_item_data(product))

        self.logger.info("Parsed {} items.".format(len(items_data)))
        return items_data

    @staticmethod
    def _extract_item_data(div):
        node = div.find('div', class_='thumbnail')

        title_link = node\
            .find('div', class_='markdown-caption')\
            .find('div', class_='item-name')\
            .find('a', class_='ec-price-item-link')

        id_match = re.search(r'/((?:[0-9a-f]+-){4}[0-9a-f]+)/', title_link['href'])
        id = id_match.group(1)
        name = title_link.text
        link = 'https://dns-shop.ru' + title_link['href']
        price_node = node.find('div', class_='item-price').find('div', class_='product-item-price').div.div.span
        price = Decimal(price_node['data-value'])
        return id, name, link, price

