import logging

from selenium import webdriver
from selenium.webdriver.firefox.options import Options

logger = logging.getLogger('app.selenium')


def construct_driver():
    firefox_profile = webdriver.FirefoxProfile()
    firefox_profile.set_preference("intl.accept_languages", "ru-RU")

    options = Options()
    options.add_argument('--headless')

    # get geckodriver here: https://github.com/mozilla/geckodriver/releases/tag/v0.26.0
    driver = webdriver.Firefox(options=options,
                               executable_path="bin/geckodriver",
                               firefox_profile=firefox_profile)
    return driver

