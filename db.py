from decimal import Decimal
from pony.orm import *


db = Database()


class Shop(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    items = Set("ShopItem")


class ShopItem(db.Entity):
    shop = Required(Shop)
    name = Required(str)
    price = Required(Decimal)
    link = Required(str)
    id = PrimaryKey(str)


db.bind(
    provider='sqlite',
    filename='db.sqlite',
    create_db=True)
db.generate_mapping(create_tables=True)

